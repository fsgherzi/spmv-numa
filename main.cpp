#include <iostream>
#include <numa.h>

#include "utils/fstypes.hpp"

#include "utils/tensor/coo_matrix.hpp"
#include "utils/tensor/csr_matrix.hpp"
#include "utils/tensor/sparse_matrix.hpp"
#include "utils/tensor/sparse_matrix_factory.hpp"

#include "utils/tensor/vector.hpp"
#include "utils/tensor/vector_factory.hpp"

#include "utils/benchmarks/spmv.hpp"



using namespace fs;

i32 main(i32 argc, c8 **argv) {
  if (argc != 6) {
    std::cout << "[USAGE] ./spmv-numa <mtx_file> <format> "
                 "<streaming_access_node_id> <random_access_node_id> "
                 "<iterations>"
              << std::endl;
    exit(-1);
  }
  u32 num_threads = 0;

  if (std::getenv("OMP_NUM_THREADS") == nullptr) {
    num_threads = 8;
  } else  {
    num_threads = std::atoll(std::getenv("OMP_NUM_THREADS"));
  }

#pragma omp parallel num_threads(num_threads) default(none)
{ 
  printf("Thread spawned\n");
}

  u32 args = 1;
  const std::string mtx_file         = argv[args++];
  const std::string matrix_format    = argv[args++];
  const u32 streaming_access_node_id = std::atoll(argv[args++]);
  const u32 random_access_node_id    = std::atoll(argv[args++]);
  const u32 iterations               = std::atoll(argv[args++]);

  const bool has_numa = numa_available() >= 0;

  if (!has_numa) {
    std::cout << "[ERROR] NUMA not available" << std::endl;
    exit(-1);
  }

  const u32 nnuma_nodes = numa_num_configured_nodes();
  if ((streaming_access_node_id >= nnuma_nodes) || (random_access_node_id >= nnuma_nodes)) {
    std::cout << "[ERROR] either src_numa_node_id(" << streaming_access_node_id
              << ") or dest_numa_node_id(" << random_access_node_id << ")"
              << " ecceded the number of available NUMA nodes on the system ("
              << nnuma_nodes << ")" << std::endl;
    exit(-1);
  }

  if (matrix_format == "csr"){
    const auto* m = SparseMatrixFactory::build<CSRMatrix<u32, f32>>(mtx_file, streaming_access_node_id);
    const auto* x = VectorFactory::build<Vector<f32>>(m->M(), random_access_node_id);
    auto* y       = VectorFactory::build<Vector<f32>>(m->N(), streaming_access_node_id);
    spmv_csr(y, m, x, iterations);
  } else {
    const auto* m = SparseMatrixFactory::build<COOMatrix<u32, f32>>(mtx_file, streaming_access_node_id);
    const auto* x = VectorFactory::build<Vector<f32>>(m->M(), random_access_node_id);
    auto* y       = VectorFactory::build<Vector<f32>>(m->N(), streaming_access_node_id);
    spmv_coo(y, m, x, iterations);
  }
}
