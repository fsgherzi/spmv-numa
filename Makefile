CXX = g++
CXXFLAGS +=-std=c++23 -O3 -g -ffast-math -fopenmp
LIBS += -lnuma

SOURCES = main.cpp utils/tensor/sparse_matrix_factory.cpp utils/tensor/coo_matrix.cpp utils/tensor/csr_matrix.cpp utils/tensor/sparse_matrix.cpp utils/tensor/vector.cpp utils/tensor/vector_factory.cpp utils/benchmarks/spmv.cpp

TARGET = spmv-numa 

all: $(TARGET) 

$(TARGET): $(SOURCES) 
	$(CXX) $(LDFLAGS) -o $(TARGET) $(CXXFLAGS) $(SOURCES) $(LIBS)

clean:
	rm -f $(TARGET) 

install:
	mkdir -p ../bin
	cp -f $(TARGET) ../bin/$(TARGET)
	cp -r matrices ../bin/matrices

test: $(TARGET)
	./$(TARGET) matrices/1138_bus.mtx csr 0 0 100
