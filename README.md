# SpMV - NUMA benchmark

## Building
Run `make` to build. Requires libnuma and openmp.

## Running
Run 
```bash
./spmv-numa <mtx_file> <format> <streaming_access_node_id> <random_access_node_id> <iterations>
```

