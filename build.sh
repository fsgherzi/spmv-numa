echo -e "\033[32mCleaning directory\033[m"
make clean
echo -e "\033[32mCompiling\033[m"
make -j8
echo -e "\033[32mInstalling\033[m"
make install
