//
// Created by lnghrdntcr on 8/1/21.
//
#pragma once

namespace fs {
	using c8  = char;
	using u32 = unsigned;
	using u64 = long long unsigned;
	using i32 = int;
	using i64 = long long int;
	using f32 = float;
	using f64 = double;
}
