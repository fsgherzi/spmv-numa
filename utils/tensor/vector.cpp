#include "vector.hpp"

template <typename val_t>
Vector<val_t>::Vector(const u32 size, const u32 numa_node_id) {
  m_data = (val_t*) numa_alloc_onnode(size * sizeof(val_t), numa_node_id);
}

template class Vector<f32>;
template class Vector<f64>;

