#include "vector_factory.hpp"

template <typename vector_t>
vector_t* VectorFactory::build(const u32 size, const u32 numa_node_id) {
  return new vector_t(size, numa_node_id);
}

template Vector<f32>* VectorFactory::build<Vector<f32>>(const u32, const u32);
template Vector<f64>* VectorFactory::build<Vector<f64>>(const u32, const u32);
