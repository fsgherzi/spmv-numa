#include "coo_matrix.hpp"
#include "sparse_matrix.hpp"
#include "sparse_matrix_factory.hpp"

std::tuple<u64, u64, u64, elements_type> SparseMatrixFactory::read_file(const std::string &file_path) {
  std::ifstream matrix_file;
  std::string header;
  u64 N, M, nnz;
  u64 x, y;
  f64 val;
  std::vector<std::tuple<u64, u64, f64>> elements;

  bool is_complex;
  bool is_symmetric;
  bool is_coordinate;

  matrix_file.open(file_path);

  if (!matrix_file.is_open()) {
    std::cout << "[ERROR] couldn't open file." << std::endl;
    exit(-1);
  }

  std::getline(matrix_file, header);

  if (!header.contains("%%MatrixMarket")) {
    std::cout << "[ERROR] Couldn't recognize format from header " << header
              << std::endl;
    exit(-1);
  }

  is_complex = header.contains("complex");
  is_symmetric = header.contains("symmetric");
  is_coordinate = header.contains("coordinate");

  if (is_complex) {
    std::cout << "[ERROR] complex matrices are not supported." << std::endl;
    exit(-1);
  }

  if (!is_coordinate) {
    std::cout << "[ERROR] non-coordinate matrices are not supported"
              << std::endl;
    exit(-1);
  }

  // Ignore all the comments
  while (header.contains("%")) {
    std::getline(matrix_file, header);
  }

  // Split the line, get N, M, nnz
  std::stringstream(header) >> N >> M >> nnz;

  while (matrix_file >> x >> y >> val) {
    if (val == 0)
      val = 1;
    elements.push_back({x, y, val});
  }

  std::sort(elements.begin(), elements.end(), [](auto v1, auto v2) {
    const auto &[x1, y1, val1] = v1;
    const auto &[x2, y2, val2] = v2;
    return x1 > x2;
  });

  // TODO: if is_symmetric && m_elements.size() != nnz
  //       add (y, x, val) at the end for everything and then sort the tuples

  return {N, M, nnz, elements};
}
/*
const SparseMatrix* SparseMatrixFactory::build_coo(const u64 N, const u64 M, const u64 nnz, const elements_type &elements, const u32 idx_type_len, const u32 value_type_len, const u32 numa_node_id) {

  if (idx_type_len == 4 && value_type_len == 4)
    return new COOMatrix<u32, f32>(N, M, nnz, elements, numa_node_id);

  if (idx_type_len == 4 && value_type_len == 8)
    return new COOMatrix<u32, f64>(N, M, nnz, elements, numa_node_id);

  if (idx_type_len == 8 && value_type_len == 4)
    return new COOMatrix<u64, f32>(N, M, nnz, elements, numa_node_id);

  if (idx_type_len == 8 && value_type_len == 8)
    return new COOMatrix<u64, f64>(N, M, nnz, elements, numa_node_id);

  ASSERT_NOT_REACHED;
}

const SparseMatrix* SparseMatrixFactory::build_csr(const u64 N, const u64 M, const u64 nnz, const elements_type &elements, const u32 idx_type_len, const u32 value_type_len, const u32 numa_node_id) {

  if (idx_type_len == 4 && value_type_len == 4)
    return new CSRMatrix<u32, f32>(N, M, nnz, elements, numa_node_id);

  if (idx_type_len == 4 && value_type_len == 8)
    return new CSRMatrix<u32, f64>(N, M, nnz, elements, numa_node_id);

  if (idx_type_len == 8 && value_type_len == 4)
    return new CSRMatrix<u64, f32>(N, M, nnz, elements, numa_node_id);

  if (idx_type_len == 8 && value_type_len == 8)
    return new CSRMatrix<u64, f64>(N, M, nnz, elements, numa_node_id);

  ASSERT_NOT_REACHED;
}
*/
template <typename matrix_type_t>
const matrix_type_t* SparseMatrixFactory::build(const std::string &file_path, const u32 numa_node_id) {
  const auto &[N, M, nnz, elements] = read_file(file_path);
  return new matrix_type_t(N, M, nnz, elements, numa_node_id);
}

template const COOMatrix<u32, f32>* SparseMatrixFactory::build<COOMatrix<u32, f32>>(const std::string&, const u32);
template const COOMatrix<u32, f64>* SparseMatrixFactory::build<COOMatrix<u32, f64>>(const std::string&, const u32);
template const COOMatrix<u64, f32>* SparseMatrixFactory::build<COOMatrix<u64, f32>>(const std::string&, const u32);
template const COOMatrix<u64, f64>* SparseMatrixFactory::build<COOMatrix<u64, f64>>(const std::string&, const u32);

template const CSRMatrix<u32, f32>* SparseMatrixFactory::build<CSRMatrix<u32, f32>>(const std::string&, const u32);
template const CSRMatrix<u32, f64>* SparseMatrixFactory::build<CSRMatrix<u32, f64>>(const std::string&, const u32);
template const CSRMatrix<u64, f32>* SparseMatrixFactory::build<CSRMatrix<u64, f32>>(const std::string&, const u32);
template const CSRMatrix<u64, f64>* SparseMatrixFactory::build<CSRMatrix<u64, f64>>(const std::string&, const u32);

