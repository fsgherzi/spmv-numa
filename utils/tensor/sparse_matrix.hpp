#pragma once

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <numa.h>
#include <ranges>
#include <sstream>
#include <string>
#include <string_view>
#include <tuple>
#include <vector>

#include "../fstypes.hpp"
#include "../fsutils.hpp"


using namespace fs;
using row_type = std::tuple<u64, u64, f64>;
using elements_type = std::vector<row_type>;

class SparseMatrix {
public:
  enum class MatrixFormat { CSR, COO };
  SparseMatrix(const u64, const u64, const u64, const elements_type &,
               const u32, const MatrixFormat);

  const u64 N() const { return m_N; };
  const u64 M() const { return m_M; };
  const u64 nnz() const { return m_nnz; };
  const MatrixFormat format() const { return m_matrix_format; };
  static MatrixFormat format_from_str(const std::string&);

protected: 
  u64 m_N;
  u64 m_M;
  u64 m_nnz;
  u32 m_numa_node_id;
  elements_type m_elements;
  bool has_valid_base_storage {true};

  void invalidate_base_storage();

private: 
  MatrixFormat m_matrix_format;

};
