#include "sparse_matrix.hpp"

SparseMatrix::SparseMatrix(const u64 N, const u64 M, const u64 nnz, const elements_type &elements, const u32 numa_node_id, const SparseMatrix::MatrixFormat matrix_format) 
  : m_N(N), m_M(M), m_nnz(nnz), m_elements(std::move(elements)), m_numa_node_id(numa_node_id), m_matrix_format(matrix_format) {}

void SparseMatrix::invalidate_base_storage() {
  m_elements.clear();
  has_valid_base_storage = false;
}

SparseMatrix::MatrixFormat SparseMatrix::format_from_str(const std::string& format) {

  if (format == "coo")
    return SparseMatrix::MatrixFormat::COO;
  if (format == "csr")
    return SparseMatrix::MatrixFormat::CSR;

  ASSERT_NOT_REACHED;

}

