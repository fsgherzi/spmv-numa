#pragma once

#include "sparse_matrix.hpp"

template <typename index_type, typename value_type>
class COOMatrix : public SparseMatrix {
public:
  using idx_t = index_type;
  using val_t = value_type;

  COOMatrix(const u64, const u64, const u64, const elements_type &, const u32);

  const index_type* x() const { return m_x; }

  const index_type* y() const { return m_y; }

  const value_type* val() const { return m_val; }

  ~COOMatrix(); 

private:
  index_type *m_x;
  index_type *m_y;
  value_type *m_val;
};
