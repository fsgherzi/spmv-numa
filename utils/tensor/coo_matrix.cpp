#include "coo_matrix.hpp"

template <typename index_type, typename value_type>
COOMatrix<index_type, value_type>::COOMatrix(const u64 N, const u64 M, const u64 nnz, const elements_type &elements, const u32 numa_node_id) 
  : SparseMatrix(N, M, nnz, elements, numa_node_id, SparseMatrix::MatrixFormat::COO) {
  m_x   = (index_type*) numa_alloc_onnode(m_nnz * sizeof(index_type), numa_node_id);
  m_y   = (index_type*) numa_alloc_onnode(m_nnz * sizeof(index_type), numa_node_id);
  m_val = (value_type*) numa_alloc_onnode(m_nnz * sizeof(value_type), numa_node_id);

  for(u64 i = 0; i < m_nnz; ++i) {
    const auto& [x, y, val] = m_elements[i];
    m_x[i]   = x;
    m_y[i]   = y;
    m_val[i] = val;
  }
  invalidate_base_storage();
}

template<typename index_type, typename value_type>
COOMatrix<index_type, value_type>::~COOMatrix() {
    free(m_x);
    free(m_y);
    free(m_val);
  }

template class COOMatrix<u32, f32>;
template class COOMatrix<u32, f64>;
template class COOMatrix<u64, f32>;
template class COOMatrix<u64, f64>;
