#pragma once

#include "../fsutils.hpp"
#include "coo_matrix.hpp"
#include "csr_matrix.hpp"
#include "sparse_matrix.hpp"

class SparseMatrixFactory {
public:
  template <typename matrix_type>
  static const matrix_type* build(const std::string &, const u32);
  static std::tuple<u64, u64, u64, elements_type> read_file(const std::string &);

private:
};
