#pragma once

#include "../fstypes.hpp"
#include "vector.hpp"

using namespace fs;

class VectorFactory {
public: 

  template <typename vector_t>
  static vector_t* build(u32, u32);

};
