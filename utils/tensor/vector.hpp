#pragma once

#include <numa.h>

#include "../fstypes.hpp"

using namespace fs;

template <typename val_t>
class Vector {

public: 
  Vector(const u32, const u32);

  const val_t* raw() const { return m_data; };
  val_t* raw() { return m_data; };
  ~Vector(){ free(m_data); }

private: 
  val_t* m_data {nullptr};

};
