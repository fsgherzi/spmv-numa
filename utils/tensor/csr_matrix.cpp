#include "csr_matrix.hpp"

template <typename index_type, typename value_type>
CSRMatrix<index_type, value_type>::CSRMatrix(const u64 N, const u64 M, const u64 nnz, const elements_type &elements, const u32 numa_node_id) 
  : SparseMatrix(N, M, nnz, elements, numa_node_id, SparseMatrix::MatrixFormat::CSR) {
  m_col_idx   = (index_type*) numa_alloc_onnode(m_nnz * sizeof(index_type), numa_node_id);
  m_row_ptr   = (index_type*) numa_alloc_onnode((N + 1) * sizeof(index_type), numa_node_id);
  m_val       = (value_type*) numa_alloc_onnode(m_nnz * sizeof(value_type), numa_node_id);


  for(u64 i = 0; i < (N + 1); ++i) 
    m_row_ptr[i] = 0;

  for(u64 i = 0; i < m_nnz; ++i) {
    const auto& [x, y, val] = m_elements[i];
    m_val[i]        = val;
    m_col_idx[i]    = y;
    m_row_ptr[x + 1]++;
  }


  for(u64 i = 0; i < N; ++i) 
    m_row_ptr[i + 1] += m_row_ptr[i];


  invalidate_base_storage();
}

template <typename index_type, typename value_type>
CSRMatrix<index_type, value_type>::~CSRMatrix() {
  free(m_col_idx);
  free(m_row_ptr);
  free(m_val);
}

template class CSRMatrix<u32, f32>;
template class CSRMatrix<u32, f64>;
template class CSRMatrix<u64, f32>;
template class CSRMatrix<u64, f64>;
