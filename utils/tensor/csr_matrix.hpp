#pragma once

#include "sparse_matrix.hpp"

template <typename index_type, typename value_type>
class CSRMatrix : public SparseMatrix {
public:
  CSRMatrix(const u64, const u64, const u64, const elements_type &, const u32);

  const index_type* col_idx() const { return m_col_idx; }

  const index_type* row_ptr() const { return m_row_ptr; }

  const value_type* val() const { return m_val; }

  ~CSRMatrix();

private:
  index_type *m_col_idx;
  index_type *m_row_ptr;
  value_type *m_val;
};
