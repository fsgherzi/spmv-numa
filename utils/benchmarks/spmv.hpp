#pragma once

#include <string>

#include "../fsutils.hpp"

#include "../tensor/coo_matrix.hpp"
#include "../tensor/csr_matrix.hpp"
#include "../tensor/vector.hpp"

#if defined(ENABLE_PARSEC_HOOKS)
#include "hooks.h"
#endif


enum SpMVAlgo {
  COO_DEFAULT, 
  CSR_DEFAULT, 
  COO_PARTITION,
  CSR_PARTITION
};

template <typename d_val_t, typename m_idx_t, typename m_val_t, typename s_val_t>
void spmv_coo_inner(d_val_t* y, const m_idx_t* col, const m_idx_t* row, const m_val_t* val, const s_val_t* x, const u64 nnz, const u32); 

template<
  template <typename> class Vector, 
  template <typename, typename> class COOMatrix,
  typename d_val_t, 
  typename m_idx_t,
  typename m_val_t,
  typename s_val_t
>
void spmv_coo(Vector<d_val_t>*, const COOMatrix<m_idx_t, m_val_t>*, const Vector<s_val_t>*, const u32);

template <typename d_val_t, typename m_idx_t, typename m_val_t, typename s_val_t>
void spmv_csr_inner(d_val_t* y, const m_idx_t* row_ptr, const m_idx_t* col_idx, const m_val_t* val, const s_val_t* x, const u64 nnz, const u32); 

template<
  template <typename> class Vector, 
  template <typename, typename> class CSRMatrix,
  typename d_val_t, 
  typename m_idx_t,
  typename m_val_t,
  typename s_val_t
>
void spmv_csr(Vector<d_val_t>*, const CSRMatrix<m_idx_t, m_val_t>*, const Vector<s_val_t>*, const u32);
