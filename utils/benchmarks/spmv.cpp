#include "spmv.hpp"

template <typename d_val_t, typename m_idx_t, typename m_val_t, typename s_val_t>
void spmv_coo_inner(d_val_t* y, const m_idx_t* col, const m_idx_t* row, const m_val_t* val, const s_val_t* x, const u64 nnz, const u32 iterations) {
  for(u32 iter = 0; iter < iterations; ++iter) {
#pragma omp parallel for schedule(static)
    for(u64 i = 0; i < nnz; ++i) {
      y[row[i]] += (val[i] * x[col[i]]);
    }
  }
}

template<
  template <typename> class Vector, 
  template <typename, typename> class COOMatrix,
  typename d_val_t, 
  typename m_idx_t,
  typename m_val_t,
  typename s_val_t
>
void spmv_coo(Vector<d_val_t>* y, const COOMatrix<m_idx_t, m_val_t>* M, const Vector<s_val_t>* x, const u32 iterations) {
  d_val_t* y_raw = y->raw();
  const auto* col = M->x();
  const auto* row = M->y();
  const auto* val = M->val();
  auto nnz = M->nnz();
  const s_val_t* x_raw = x->raw();
#ifdef ENABLE_PARSEC_HOOKS
    __parsec_roi_begin();
#endif
  spmv_coo_inner(y_raw, col, row, val, x_raw, nnz, iterations);
#ifdef ENABLE_PARSEC_HOOKS
    __parsec_roi_end();
#endif
}

template <typename d_val_t, typename m_idx_t, typename m_val_t, typename s_val_t>
void spmv_csr_inner(d_val_t* y, const m_idx_t* row_ptr, const m_idx_t* col_idx, const m_val_t* val, const s_val_t* x, const u64 N, const u32 iterations) {

  for(u32 iter = 0; iter < iterations; ++iter) {

#pragma omp parallel for schedule(dynamic)
    for (u64 i = 0; i < N; ++i) {
      m_idx_t begin = row_ptr[i];
      m_idx_t end   = row_ptr[i];
  
      d_val_t acc = 0.0;
      for (u32 j = begin; j < end; ++j) {
        acc += val[j] * x[col_idx[i]];
      }
  
      y[i] = acc;
   }
  }
};

template<
  template <typename> class Vector, 
  template <typename, typename> class CSRMatrix,
  typename d_val_t, 
  typename m_idx_t,
  typename m_val_t,
  typename s_val_t
>
void spmv_csr(Vector<d_val_t>* y, const CSRMatrix<m_idx_t, m_val_t>* M, const Vector<s_val_t>* x, const u32 iterations) {

  d_val_t* y_raw = y->raw();
  const auto* col = M->col_idx();
  const auto* row_ptr = M->row_ptr();
  const auto* val = M->val();
  auto N = M->N();
  const s_val_t* x_raw = x->raw();
#ifdef ENABLE_PARSEC_HOOKS
    __parsec_roi_begin();
#endif
  spmv_csr_inner(y_raw, row_ptr, col, val, x_raw, N, iterations);
#ifdef ENABLE_PARSEC_HOOKS
    __parsec_roi_end();
#endif
}

template void spmv_coo<Vector, COOMatrix, f32, u32, f32, f32>(Vector<f32>*, const COOMatrix<u32, f32>*, const Vector<f32>*, const u32);
template void spmv_csr<Vector, CSRMatrix, f32, u32, f32, f32>(Vector<f32>*, const CSRMatrix<u32, f32>*, const Vector<f32>*, const u32);
